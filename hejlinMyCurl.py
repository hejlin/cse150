from socket import *
import argparse
import re
import sys
import os

parser = argparse.ArgumentParser()
parser.add_argument('url1')
parser.add_argument('url2', nargs = '?', default = 'default_flag')

args = parser.parse_args()

#The follow two pieces of logic makes sure that a valid full URL is used as
#the first argument or else there is an error message and the program exits
if 'https://' in args.url1 or 'https://' in args.url2:
  sys.exit('This program does not support HTTPS, please use a different URL.')
else:
  pass
if 'http://' not in args.url1:
  sys.exit('Please input a valid full URL.')
else:
  pass

#Splits the full URL by the various things, http://, :, and /
#Then removes the first item in the splitted list because there is an empty spot
split = re.split('http://|\:|\/', args.url1)
if '' in split:
  split.remove('')

#Checks to see if the port number was included, or else set port to a defaulted 80.
#Also removes the port number from the split list so it doesnt interfere.
for item in split:
  try:
    if int(item):
      port = item
      split.remove(item)
  except ValueError:
    port = 80

#Sometimes if you put a single number like http://19 for the ip address by accident
#it would still go through so this checks if it was removed by the previous logic
#and if so, it would cause an error message to appear.
if len(split) == 0:
  sys.exit('Please input a valid argument.')

#This checks for any 'requests' that were included like /foo.html and will set
#a variable with that request, else just set the host name.
if len(split) != 1:
  hostname = split[0]
  request = split[1]
  #print(request)
else:
  hostname = split[0]
  request = None

#This checks if the first argument was an IP address. If it was, then we set the
#IP address as the first argument and the second argument as the host name.
#This also checks if a second argument was given which errors if there was none.
#Also overrides the host name set in the previous logic.
#If the IP address wasn't given in the first argument, we can get by using
#gethostbyname and the host name.
periods = len(re.findall('\.', split[0]))
if periods == 3:
  ipaddr = split[0]
  if args.url2 != 'default_flag':
    hostname = args.url2
  else:
    sys.exit('Please input a host name as a second argument if you are putting an IP address as the first argument.')
else:
  hostname = split[0]
  ipaddr = gethostbyname(hostname)




socket = socket(AF_INET, SOCK_STREAM)
#socket.settimeout(5)
try:
  socket.settimeout(10)
  socket.connect((hostname, int(port)))
except:
  print('Connection Timed Out')
  socket.close()
  sys.exit()


if request != None:
  datamsg = 'GET /' + request + ' HTTP/1.1\r\nHost: ' + hostname + '\r\n\r\n'
else:
  datamsg = 'GET /' + ' HTTP/1.1\r\nHost: ' + hostname + '\r\n\r\n'

socket.send(datamsg.encode())
datarecv = socket.recv(4000000)



decoded = datarecv.decode("ISO-8859-1")
decodedlist = decoded.splitlines()
srcip = socket.getsockname()

  
if len(decodedlist) != 0:
  split = re.split(' ', decodedlist[0])
  for item in split:
    try:
      if int(item):
        statusnum = item
    except:
      pass
else:
  statusnum = '56'

socket.close()

#HTTPoutput Data
if statusnum == '200' and 'Transfer-Encoding: chunked' not in decodedlist:
  dataoutput = open('HTTPoutput.html', 'w')
  
  #Gets the index of the starting point of actual data
  for item in decodedlist:
    if item == '':
      index = decodedlist.index(item)
      break
  
  #print(index)
  #print(decodedlist[index+1:])
  
  for item in decodedlist[index + 1:]:
    if item != '':
      dataoutput.write(item)
      dataoutput.write('\n')
    else:
      dataoutput.write('\n')
      dataoutput.write('\n')

  print('Success', args.url1, decodedlist[0])
  dataoutput.close()
else:
  if len(decodedlist) != 0:
    print('Unsuccessful', args.url1, decodedlist[0])
  else:
    print('Unsuccessful', args.url1, '[Errno 54] Connection reset by peer')


#Log.csv Logic
log = open("Log.csv", "a")
logsize = 'Log.csv'
if os.stat(logsize).st_size == 0:
  log.write('Successful or Unsuccessful, ')
  log.write('Server Status Code, ')
  log.write('Requested URL, ')
  log.write('hostname, ')
  log.write('source IP, ')
  log.write('destination IP, ')
  log.write('source port, ')
  log.write('destination port, ')
  log.write('Server Response line (including code and phrase)')
  log.write('\n')
  log.write('\n')

if 'Transfer-Encoding: chunked' in decodedlist or statusnum != '200' or statusnum == '56':
  if 'Transfer-Encoding: chunked' in decodedlist:
    print('Chunk encoding is not supported.')
  log.write('Unsuccessful, ')
else:
  log.write('Successful, ')
  
log.write(statusnum)
log.write(', ')
log.write(args.url1)
log.write(', ')

if args.url2 != 'default_flag': 
  log.write(args.url2)
else:
  log.write(hostname)
  
log.write(', ')
log.write(srcip[0])
log.write(', ')
log.write(ipaddr)
log.write(', ')
log.write(str(srcip[1]))
log.write(', ')
log.write(str(port))
log.write(', ')

if len(decodedlist) != 0:
  log.write(decodedlist[0])
else:
  log.write('[Errno 54] Connection reset by peer')
  
log.write('\n')
log.write('\n')
log.close()
    


#print(decoded)
#print(decodedlist)
#print(srcip)
# print(datamsg)
# print(ipaddr)
# print(hostname)
# print(port)
# print(split)
 


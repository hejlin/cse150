#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.cli import CLI

class MyTopology(Topo):
    """
    A basic topology
    """
    def __init__(self):
        Topo.__init__(self)

        # Set Up Topology Here
        switch1 = self.addSwitch('Switch1')    ## Adds a Switch
        switch2 = self.addSwitch('Switch2')
        switch3 = self.addSwitch('Switch3')

        host1 = self.addHost('User1')       ## Adds a Host
        host2 = self.addHost('User2')
        host3 = self.addHost('Laptop')
        host4 = self.addHost('Phone')
        host5 = self.addHost('Ipad')
        host6 = self.addHost('Server1')
        host7 = self.addHost('Server2')

        self.addLink(host1, switch1)      ## Add a link
        self.addLink(host2, switch1)
        self.addLink(host3, switch1)
        self.addLink(switch1, switch2)
        self.addLink(host4, switch2)
        self.addLink(host5, switch2)
        self.addLink(switch2, switch3)
        self.addLink(host6, switch3)
        self.addLink(host7, switch3)

if __name__ == '__main__':
    """
    If this script is run as an executable (by chmod +x), this is
    what it will do
    """

    topo = MyTopology()   		 ## Creates the topology
    net = Mininet( topo=topo )   	 ## Loads the topology
    net.start()                      ## Starts Mininet

    # Commands here will run on the simulated topology
    CLI(net)

    net.stop()                       ## Stops Mininet

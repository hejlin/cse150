#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
from mininet.cli import CLI
from mininet.node import RemoteController

class prelab3_topo(Topo):
  def build(self):
    switch1 = self.addSwitch('switch1')
    switch2 = self.addSwitch('switch2')
    switch3 = self.addSwitch('switch3')

    client1 = self.addHost('client1', ip='20.1.1.10/24')
    client2 = self.addHost('client2', ip='20.1.1.11/24')
    client3 = self.addHost('client3', ip='20.1.10.1/24')
    client4 = self.addHost('client4', ip='20.1.1.30/24')
    client5 = self.addHost('client5', ip='20.1.1.31/24')

    self.addLink(switch1, switch2)
    self.addLink(switch1, switch3)
    self.addLink(client1, switch1)
    self.addLink(client2, switch1)
    self.addLink(client3, switch2)
    self.addLink(client4, switch3)
    self.addLink(client5, switch3)
    

def configure():
  topo = prelab3_topo()
  net = Mininet(topo=topo, controller=RemoteController)
  net.start()
  CLI(net)
  net.stop()

if __name__ == '__main__':
  configure()

# Lab 3 Skeleton
#
# Based on of_tutorial by James McCauley
from pox.core import core
import pox.openflow.libopenflow_01 as of
log = core.getLogger()
class Firewall (object):
  """
  A Firewall object is created for each switch that connects.
  A Connection object for that switch is passed to the __init__ function.
  """
  def __init__ (self, connection):
    # Keep track of the connection to the switch so that we can
    # send it messages!
    self.connection = connection
    # This binds our PacketIn event listener
    connection.addListeners(self)
  def do_firewall (self, packet, packet_in):
    # The code in here will be executed for every packet.
    TCP = packet.find("tcp")
    ARP = packet.find("arp")
    ICMP = packet.find("icmp")

    msg = of.ofp_flow_mod()
    msg.match = of.ofp_match.from_packet(packet)
    msg.match = of.ofp_match(dl_type = 0x0800, nw_proto = 1)
    msg.match = of.ofp_match(dl_type = 0x0806)
    msg.match = of.ofp_match(nw_proto = 6, nw_src = "20.1.1.10", nw_dst = "20.1.1.55")
    msg.match = of.ofp_match(nw_proto = 6, nw_src = "20.1.1.55", nw_dst = "20.1.1.10")
    msg.match = of.ofp_match(nw_proto = 6, nw_src = "20.1.1.11", nw_dst = "20.1.1.55")
    msg.match = of.ofp_match(nw_proto = 6, nw_src = "20.1.1.55", nw_dst = "20.1.1.11")
    msg.match = of.ofp_match(nw_proto = 6, nw_src = "20.1.1.55", nw_dst = "20.1.1.30")
    msg.match = of.ofp_match(nw_proto = 6, nw_src = "20.1.1.55", nw_dst = "20.1.1.31")
    msg.match = of.ofp_match(nw_proto = 6, nw_src = "20.1.1.31", nw_dst = "20.1.1.55")

    if not TCP and not ARP and not ICMP:
      self.connection.send(msg)
    else:
      msg.actions.append(of.ofp_action_output(port=of.OFPP_FLOOD))
      self.connection.send(msg)


  def _handle_PacketIn (self, event):
    """
    Handles packet in messages from the switch.
    """
    packet = event.parsed # This is the parsed packet data.
    if not packet.parsed:
      log.warning("Ignoring incomplete packet")
      return
    packet_in = event.ofp # The actual ofp_packet_in message.
    self.do_firewall(packet, packet_in)
def launch ():
  """
  Starts the component
  """
  def start_switch (event):
    log.debug("Controlling %s" % (event.connection,))
    Firewall(event.connection)
  core.openflow.addListenerByName("ConnectionUp", start_switch)